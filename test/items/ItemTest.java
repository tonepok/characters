package items;

import characters.charactertypes.Warrior;
import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.itemtypes.Armor;
import items.itemtypes.Weapon;
import org.junit.jupiter.api.Test;
import util.PrimaryAttributes;
import util.Slot;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void equipWeapon_tooHighLevel_ShouldThrowInvalidWeaponException() {
        //Arrange
        Warrior gimli = new Warrior("Gimli");
        Weapon axe = new Weapon("Battle axe", 2, WeaponType.axe, 3, 1.2);
        //Act and Assert
        assertThrows(InvalidWeaponException.class, ()-> gimli.equip(axe));
    }

    @Test
    void equipArmor_tooHighLevel_ShouldThrowInvalidArmorException() {
        Warrior gimli = new Warrior("Gimli");
        Armor plate = new Armor("Old plate", 2, ArmorType.plate, Slot.body, new PrimaryAttributes(3,1,4,1));
        assertThrows(InvalidArmorException.class, ()-> gimli.equip(plate));
    }

    @Test
    void equipWeapon_wrongType_ShouldThrowInvalidWeaponException() {
        Warrior gimli = new Warrior("Gimli");
        Weapon bow = new Weapon("Bow of sticks", 1, WeaponType.bow, 3, 1.2);
        assertThrows(InvalidWeaponException.class, ()-> gimli.equip(bow));
    }

    @Test
    void equipArmor_wrongType_ShouldThrowInvalidArmorException() {
        Warrior gimli = new Warrior("Gimli");
        Armor oldCloth = new Armor("Cloth rag", 1, ArmorType.cloth, Slot.body, new PrimaryAttributes(3,1,4,1));
        assertThrows(InvalidArmorException.class, ()-> gimli.equip(oldCloth));
    }

    @Test
    void equipWeapon_validType_ShouldReturnTrue() {
        Warrior gimli = new Warrior("Gimli");
        Weapon axe = new Weapon("Battle axe", 1, WeaponType.axe, 3, 1.2);
        try {
            assertTrue(gimli.equip(axe));
        }
        catch (InvalidWeaponException | InvalidArmorException e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void equipArmor_validType_ShouldReturnTrue() {
        Warrior gimli = new Warrior("Gimli");
        Armor plate = new Armor("Old plate", 1, ArmorType.plate, Slot.body, new PrimaryAttributes(3,1,4,1));
        try {
            assertTrue(gimli.equip(plate));
        }
        catch (InvalidWeaponException | InvalidArmorException e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void calculateDPS_NoWeapon_ShouldBeCorrectValue() {
        Warrior gimli = new Warrior("Gimli");
        double expected =  1 * (1 + (5 / 100));
        double actual = gimli.DPS();
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_ValidWeapon_ShouldBeCorrectValue(){
        Warrior gimli = new Warrior("Gimli");
        Weapon axe = new Weapon("Battle axe", 1, WeaponType.axe, 7, 1.1);
        double expected  = (7 * 1.1)*(1 + (5 / 100));
        try {
            gimli.equip(axe);
        }
        catch (InvalidWeaponException | InvalidArmorException e){
            System.out.println(e.getMessage());
        }
        double actual = gimli.DPS();
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_ValidWeaponAndArmor_ShouldBeCorrectValue(){
        Warrior gimli = new Warrior("Gimli");
        Weapon axe = new Weapon("Battle axe", 1, WeaponType.axe, 7, 1.1);
        Armor plate = new Armor("Old plate", 1, ArmorType.plate, Slot.body, new PrimaryAttributes(3,1,4,1));
        double expected = (7 * 1.1) * (1 + ((5+1) / 100));
        try {
            gimli.equip(axe);
            gimli.equip(plate);
        }
        catch (InvalidWeaponException | InvalidArmorException e){
            System.out.println(e.getMessage());
        }
        double actual = gimli.DPS();
        assertEquals(expected, actual);
    }
}