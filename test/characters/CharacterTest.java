package characters;

import characters.charactertypes.Mage;
import characters.charactertypes.Ranger;
import characters.charactertypes.Rogue;
import characters.charactertypes.Warrior;
import org.junit.jupiter.api.Test;
import util.PrimaryAttributes;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void LevelOneOnCreate_NewWarrior_ShouldBeLevelOne() {
        //Arrange
        Warrior aragorn = new Warrior("Aragorn");
        int expected = 1;
        //Act
        int actual = aragorn.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void LevelUpFromOne_LevelUp_ShouldBeLevelTwo() {
        //Arrange
        Warrior aragorn = new Warrior("Aragorn");
        aragorn.levelUp();
        int expected = 2;
        //Act
        int actual = aragorn.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void CreateMage_ValidArguments_ShouldHaveCorrectAttributes() {
        //Arrange
        Mage arwen = new Mage("Arwen");
        int vitality = 5;
        int strength = 1;
        int dexterity = 1;
        int intelligence = 8;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        //Act
        int[] actual = arwen.getPrimaryAttributes().getAll();
        //Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void CreateRanger_ValidArguments_ShouldHaveCorrectAttributes() {
        Ranger bilbo = new Ranger("Bilbo");
        int vitality = 8;
        int strength = 1;
        int dexterity = 7;
        int intelligence = 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = bilbo.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }

    @Test
    void CreateRogue_ValidArguments_ShouldHaveCorrectAttributes() {
        Rogue galadriel = new Rogue("Galadriel");
        int vitality = 8;
        int strength = 2;
        int dexterity = 6;
        int intelligence = 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = galadriel.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }

    @Test
    void CreateWarrior_ValidArguments_ShouldHaveCorrectAttributes() {
        Warrior aragorn = new Warrior("Aragorn");
        int vitality = 10;
        int strength = 5;
        int dexterity = 2;
        int intelligence = 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = aragorn.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }

    @Test
    void LevelTwoMage_NothingEquipped_ShouldHaveCorrectAttributes() {
        //Arrange
        Mage arwen = new Mage("Arwen");
        arwen.levelUp();
        int vitality = 5 + 3;
        int strength = 1 + 1;
        int dexterity = 1 + 1;
        int intelligence = 8 + 5;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        //Act
        int[] actual = arwen.getPrimaryAttributes().getAll();
        //Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void LevelTwoRanger_NothingEquipped_ShouldHaveCorrectAttributes() {
        Ranger bilbo = new Ranger("Bilbo");
        bilbo.levelUp();
        int vitality = 8 + 2;
        int strength = 1 + 1;
        int dexterity = 7 + 5;
        int intelligence = 1 + 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = bilbo.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }

    @Test
    void LevelTwoRogue_NothingEquipped_ShouldHaveCorrectAttributes() {
        Rogue galadriel = new Rogue("Galadriel");
        galadriel.levelUp();
        int vitality = 8 + 3;
        int strength = 2 + 1;
        int dexterity = 6 + 4;
        int intelligence = 1 + 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = galadriel.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }

    @Test
    void LevelTwoWarrior_NothingEquipped_ShouldHaveCorrectAttributes() {
        Warrior aragorn = new Warrior("Aragorn");
        aragorn.levelUp();
        int vitality = 10 + 5;
        int strength = 5 + 3;
        int dexterity = 2 + 2;
        int intelligence = 1 + 1;
        int[] expected = new PrimaryAttributes(vitality, strength, dexterity, intelligence).getAll();
        int[] actual = aragorn.getPrimaryAttributes().getAll();
        assertArrayEquals(expected, actual);
    }





}