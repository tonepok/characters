# Character application

The application lets you create a character; either a mage, rogue, ranger og warrior. These characters can all do the same things, but have different attributes - which means that they deal increased damage on different things and have different qualities. 

The application also have items which can be either a weapon or armor. What kind of items you can use depends on the character and on your level. When you equip an item the qualities from it is added to your characters qualities. 