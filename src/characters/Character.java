package characters;

import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.itemtypes.Armor;
import items.Item;
import items.itemtypes.Weapon;
import util.PrimaryAttributes;
import util.Slot;
import java.text.DecimalFormat;
import java.util.HashMap;

public abstract class Character {
    protected int level;
    protected final String name;
    protected HashMap<Slot, Item> equipment = new HashMap<Slot, Item>(4);
    protected PrimaryAttributes primaryAttributes;

    public Character(String name){
        this.name=name;
        level=1;
    }

    public abstract void levelUp();
    protected abstract int getTotalBase();
    public abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;
    public abstract boolean equipArmor(Armor armor) throws InvalidArmorException;
    public abstract String getHeroType();

    //common method so that a character can eqip an item without knowing if its an armor or a weapon
    public boolean equip(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (item.getItemType().equals("weapon")){
            return equipWeapon((Weapon) item);
        }
        else {
            return equipArmor((Armor) item);
        }
    }

    //returns the damage per second
    public double DPS(){
        double weaponDPS = 1; //if nothing is equipped
        if(equipment.get(Slot.weapon) instanceof Weapon){
            weaponDPS = ((Weapon) equipment.get(Slot.weapon)).DPS();
        }
        return weaponDPS * (1 + getTotalBase() / 100);
    }

    //prints the damage per second in a good format
    public void dealDamage(){
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("Damage dealt: "+df.format(DPS()));
    }

    public String getName() {
        return name;
    }

    public PrimaryAttributes getPrimaryAttributes(){
        return primaryAttributes;
    }

    public int getTotalDexterity(){
        int totalDexterity = primaryAttributes.getDexterity();
        if(equipment.get(Slot.head) instanceof Armor){
            totalDexterity += ((Armor) equipment.get(Slot.head)).getDexterity();
        }
        if(equipment.get(Slot.body) instanceof Armor){
            totalDexterity += ((Armor) equipment.get(Slot.body)).getDexterity();
        }
        if(equipment.get(Slot.legs) instanceof Armor){
            totalDexterity += ((Armor) equipment.get(Slot.legs)).getDexterity();
        }
        return totalDexterity;
    }

    public int getLevel() {
        return level;
    }

    public int getTotalVitality(){
        int totalVitality = primaryAttributes.getVitality();
        if(equipment.get(Slot.head) instanceof Armor){
            totalVitality += ((Armor) equipment.get(Slot.head)).getVitality();
        }
        if(equipment.get(Slot.body) instanceof Armor){
            totalVitality += ((Armor) equipment.get(Slot.body)).getVitality();
        }
        if(equipment.get(Slot.legs) instanceof Armor){
            totalVitality += ((Armor) equipment.get(Slot.legs)).getVitality();
        }
        return totalVitality;
    }

    public int getTotalStrength(){
        int totalStrength = primaryAttributes.getStrength();
        if(equipment.get(Slot.head) instanceof Armor){
            totalStrength += ((Armor) equipment.get(Slot.head)).getStrength();
        }
        if(equipment.get(Slot.body) instanceof Armor){
            totalStrength += ((Armor) equipment.get(Slot.body)).getStrength();
        }
        if(equipment.get(Slot.legs) instanceof Armor){
            totalStrength += ((Armor) equipment.get(Slot.legs)).getStrength();
        }
        return totalStrength;
    }

    public int getTotalIntelligence(){
        int totalIntelligence = primaryAttributes.getIntelligence();
        if(equipment.get(Slot.head) instanceof Armor){
            totalIntelligence += ((Armor) equipment.get(Slot.head)).getIntelligence();
        }
        if(equipment.get(Slot.body) instanceof Armor){
            totalIntelligence += ((Armor) equipment.get(Slot.body)).getIntelligence();
        }
        if(equipment.get(Slot.legs) instanceof Armor){
            totalIntelligence += ((Armor) equipment.get(Slot.legs)).getIntelligence();
        }
        return totalIntelligence;
    }

    //this prints the characters stats in a nice format
    @Override
    public String toString(){
        DecimalFormat df = new DecimalFormat("#.00");
        String toPrint = ("****** "+
                getName()+" ******\n* Level "
                +getLevel()+" "+getHeroType()+"\n* ------------------\n* Strength: "
                +getTotalStrength()+"\n* Dexterity: "
                +getTotalDexterity()+"\n* Intelligence: "+getTotalIntelligence()+
                "\n* Vitality: "+getTotalVitality()+"\n* Damage Per Second: "+df.format(DPS())+"\n*******************");
        return toPrint;
    }
}
