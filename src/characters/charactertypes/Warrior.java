package characters.charactertypes;

import characters.Character;
import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.itemtypes.Armor;
import items.itemtypes.Weapon;
import util.PrimaryAttributes;
import static items.ArmorType.*;
import static items.WeaponType.*;

public class Warrior extends Character {

    public Warrior(String name){
        super(name);
        level=1;
        primaryAttributes = new PrimaryAttributes(10, 5, 2, 1);
    }

    @Override
    protected int getTotalBase(){
        return getTotalStrength();
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getType() != axe && weapon.getType() != hammer && weapon.getType() != sword){
            throw new InvalidWeaponException("A warrior cannot use that type of weapon.");
        }
        else if(weapon.getRequiredLevel() > getLevel()){
            throw new InvalidWeaponException("Your level is too low to use this weapon.");
        }
        equipment.put(weapon.getSlot(), weapon);
        System.out.println(getName()+" equipped "+weapon.getName());
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getType() != mail && armor.getType() != plate){
            throw new InvalidArmorException("A warrior cannot use that type of armor.");
        }
        else if(armor.getRequiredLevel() > getLevel()){
            throw new InvalidArmorException("Your level is too low to use this armor.");
        }
        equipment.put(armor.getSlot(), armor);
        System.out.println(getName()+" equipped "+armor.getName()+" on "+armor.getSlot());
        return true;
    }

    @Override
    public void levelUp(){
        level++;
        primaryAttributes.setVitality((primaryAttributes.getVitality()+5));
        primaryAttributes.setStrength((primaryAttributes.getStrength()+3));
        primaryAttributes.setDexterity((primaryAttributes.getDexterity()+2));
        primaryAttributes.setIntelligence((primaryAttributes.getIntelligence()+1));
        System.out.println(getName()+" leveled up to level "+getLevel());
    }

    @Override
    public  String getHeroType(){
        return "warrior";
    }
}