package characters.charactertypes;

import characters.Character;
import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.itemtypes.Armor;
import items.itemtypes.Weapon;
import util.PrimaryAttributes;
import static items.ArmorType.leather;
import static items.ArmorType.mail;
import static items.WeaponType.*;

public class Ranger extends Character {

    public Ranger(String name){
        super(name);
        level=1;
        primaryAttributes = new PrimaryAttributes(8, 1, 7, 1);
    }

    @Override
    protected int getTotalBase(){
        return getTotalDexterity();
    }

    @Override
    public void levelUp(){
        level++;
        primaryAttributes.setVitality((primaryAttributes.getVitality()+2));
        primaryAttributes.setStrength((primaryAttributes.getStrength()+1));
        primaryAttributes.setDexterity((primaryAttributes.getDexterity()+5));
        primaryAttributes.setIntelligence((primaryAttributes.getIntelligence()+1));
        System.out.println(getName()+" leveled up to level "+getLevel());
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getType() != bow){
            throw new InvalidWeaponException("A ranger cannot use that type of weapon.");
        }
        else if(weapon.getRequiredLevel() > getLevel()){
            throw new InvalidWeaponException("Your level is too low to use this weapon.");
        }
        equipment.put(weapon.getSlot(), weapon);
        System.out.println(getName()+" equipped "+weapon.getName());
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getType() != leather && armor.getType() != mail){
            throw new InvalidArmorException("A ranger cannot use that type of armor.");
        }
        else if(armor.getRequiredLevel() > getLevel()){
            throw new InvalidArmorException("Your level is too low to use this armor.");
        }
        equipment.put(armor.getSlot(), armor);
        System.out.println(getName()+" equipped "+armor.getName()+" on "+armor.getSlot());
        return true;
    }

    @Override
    public  String getHeroType(){
        return "ranger";
    }
}