package characters.charactertypes;

import characters.Character;
import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.itemtypes.Armor;
import items.itemtypes.Weapon;
import util.PrimaryAttributes;
import static items.ArmorType.*;
import static items.WeaponType.*;

public class Rogue extends Character {

    public Rogue(String name){
        super(name);
        level=1;
        primaryAttributes = new PrimaryAttributes(8, 2, 6, 1);
    }

    @Override
    protected int getTotalBase(){
        return getTotalDexterity();
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getType() != dagger && weapon.getType() != sword){
            throw new InvalidWeaponException("A rogue cannot use that type of weapon.");
        }
        else if(weapon.getRequiredLevel() > getLevel()){
            throw new InvalidWeaponException("Your level is too low to use this weapon.");
        }
        equipment.put(weapon.getSlot(), weapon);
        System.out.println(getName()+" equipped "+weapon.getName());
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getType() != leather && armor.getType() != mail){
            throw new InvalidArmorException("A rogue cannot use that type of armor.");
        }
        else if(armor.getRequiredLevel() > getLevel()){
            throw new InvalidArmorException("Your level is too low to use this armor.");
        }
        equipment.put(armor.getSlot(), armor);
        System.out.println(getName()+" equipped "+armor.getName()+" on "+armor.getSlot());
        return true;
    }

    @Override
    public void levelUp(){
        level++;
        primaryAttributes.setVitality((primaryAttributes.getVitality()+3));
        primaryAttributes.setStrength((primaryAttributes.getStrength()+1));
        primaryAttributes.setDexterity((primaryAttributes.getDexterity()+4));
        primaryAttributes.setIntelligence((primaryAttributes.getIntelligence()+1));
        System.out.println(getName()+" leveled up to level "+getLevel());
    }

    @Override
    public  String getHeroType(){
        return "rogue";
    }
}