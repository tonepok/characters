import characters.Character;
import characters.charactertypes.Mage;
import characters.exceptions.InvalidArmorException;
import characters.exceptions.InvalidWeaponException;
import items.*;
import items.itemtypes.Armor;
import items.itemtypes.Weapon;
import util.PrimaryAttributes;
import util.Slot;

class Main {
    public static void main(String[] args) {
        Character myMage = new Mage("Mozart");
        System.out.println(myMage);
        myMage.dealDamage();

        Armor oldCloth = new Armor("Old plate", 2, ArmorType.cloth, Slot.body, new PrimaryAttributes(3,1,4,1));

        equip(myMage, oldCloth);
        myMage.levelUp();
        System.out.println(myMage);

        Weapon wand = new Weapon("The elder wand", 2, WeaponType.wand, 3, 1.2);
        equip(myMage, wand);
        System.out.println(myMage);

    }

    private static void equip(Character c, Item i){
        try {
            c.equip(i);
        }
        catch (InvalidWeaponException | InvalidArmorException e){
            System.out.println(e.getMessage());
        }
    }
}