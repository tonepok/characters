package items;

public enum ArmorType {
    cloth,
    leather,
    mail,
    plate
}
