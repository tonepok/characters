package items.itemtypes;

import items.Item;
import items.WeaponType;
import util.Slot;

public class Weapon extends Item {
    private final WeaponType type;
    private final int damage;
    private final double attackPerSecond;

    //I have all of these in the constructor because it can not exist a weapon without all of these
    public Weapon(String name, int requiredLevel, WeaponType type, int damage, double attackPerSecond){
        super(name, requiredLevel);
        this.type = type;
        this.damage = damage;
        this.attackPerSecond = attackPerSecond;
        slot = Slot.weapon;
    }

    @Override
    public String getItemType(){
        return "weapon";
    }

    public double DPS(){
        return damage * attackPerSecond;
    }

    public WeaponType getType() {
        return type;
    }

}
