package items.itemtypes;

import items.ArmorType;
import items.Item;
import util.PrimaryAttributes;
import util.Slot;

public class Armor extends Item {
    private final ArmorType type;
    public PrimaryAttributes attributes;

    //I have all of these in the constructor because it can not exist an armor without all of these
    public Armor(String name, int requiredLevel, ArmorType type, Slot slot2, PrimaryAttributes attributes){
        super(name, requiredLevel);
        this.type=type;
        slot = slot2;
        this.attributes=attributes;
    }

    @Override
    public String getItemType(){
        return "armor";
    }

    public ArmorType getType() {
        return type;
    }

    public int getVitality() {
        return attributes.getVitality();
    }

    public int getStrength() {
        return attributes.getStrength();
    }

    public int getDexterity() {
        return attributes.getDexterity();
    }

    public int getIntelligence() {
        return attributes.getIntelligence();
    }

}
