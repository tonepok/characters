package items;

public enum WeaponType {
    axe,
    bow,
    dagger,
    hammer,
    staff,
    sword,
    wand
}
