package items;

import util.Slot;

//the abstract class item which armor and weapon inherit from
public abstract class Item {
    protected final String name;
    protected final int requiredLevel;
    protected Slot slot;

    public Item(String name, int requiredLevel){
        this.name=name;
        this.requiredLevel = requiredLevel;
    }

    abstract public String getItemType();

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

}
