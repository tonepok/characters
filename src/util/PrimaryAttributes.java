package util;

public class PrimaryAttributes {
    private int strength, dexterity, vitality, intelligence;
    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence){
        this.dexterity=dexterity;
        this.strength=strength;
        this.vitality=vitality;
        this.intelligence=intelligence;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int[] getAll(){
        int all[] = {vitality, strength, dexterity, intelligence};
        return  all;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getVitality() {
        return vitality;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
