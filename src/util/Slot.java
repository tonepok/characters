package util;

public enum Slot {
    head,
    body,
    legs,
    weapon
}
